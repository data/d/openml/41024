# OpenML dataset: food-truck-test

https://www.openml.org/d/41024

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Adriano Rivolli  
**Source**: Market Research- 2017  
**Please cite**: Rivolli A., Parker L.C., de Carvalho A.C.P.L.F. (2017) Food Truck Recommendation Using Multi-label Classification. In: Oliveira E., Gama J., Vale Z., Lopes Cardoso H. (eds) Progress in Artificial Intelligence. EPIA 2017. Lecture Notes in Computer Science, vol 10423. Springer, Cham  

A multi-label dataset for food-truck classification
The referenced paper contains the details of the data set.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41024) of an [OpenML dataset](https://www.openml.org/d/41024). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41024/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41024/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41024/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

